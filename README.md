# Libertinus Fonts

This repo allows for use of the
[Libertinus](https://github.com/alerque/libertinus) fonts with CSS!

Just include the following code in the head of your HTML:

```html
<link href="https://john_t.gitlab.io/libertinus_css/libertinus.css" rel="stylesheet">
```

Or directly into CSS

```css
@import url("https://john_t.gitlab.io/libertinus_css/libertinus.css");
```
